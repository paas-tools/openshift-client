# Openshift Client

This repository contains a basic Docker Image with the Openshift 4 command-line client.
It is intended for programmatic use (e.g. in CI) or lightweight interactive use.

Available tools:

* [oc](https://docs.okd.io/4.8/cli_reference/openshift_cli/getting-started-cli.html) -- OpenShift Client
* [jq](https://manpages.debian.org/bullseye/jq/jq.1.en.html) -- JSON Processor
* [yq](https://mikefarah.gitbook.io/yq/) -- YAML Processor
* [git](https://manpages.debian.org/bullseye/git-man/git.1.en.html) -- Distributed Version Control System
* [gettext utilities](https://www.gnu.org/software/gettext/gettext.html) -- text manipulation helpers
* [bash](https://www.gnu.org/software/bash/) -- Advanced shell
* [helm](https://helm.sh/) -- Package manager for Kubernetes

*relying on any other tools is unsupported as they might be removed in the future*
