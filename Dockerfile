FROM gitlab-registry.cern.ch/linuxsupport/alma9-base:latest

# Download and verify oc CLI
# https://mirror.openshift.com/pub/openshift-v4/x86_64/clients/ocp/
ARG OC_VERSION=4.14.3
ARG OC_SHA256=1169c772230e2bc1d94662f67d1f8e75c3e295bd01582986eaa07e23f7608498
RUN cd /tmp && \
    curl "https://mirror.openshift.com/pub/openshift-v4/x86_64/clients/ocp/${OC_VERSION}/openshift-client-linux-${OC_VERSION}.tar.gz" -o oc.tar.gz && \
    echo "${OC_SHA256}  oc.tar.gz" | sha256sum -c - && \
    tar xzvf oc.tar.gz && \
    mv oc /usr/local/bin/ && \
    rm -rf /tmp/*

# Install additional useful tools
RUN yum install -y \
    bash-completion \
    gettext \
    git \
    rsync \
    jq \
    && yum clean all && rm -rf /var/cache/yum/

# Download and verify yq CLI
# https://github.com/mikefarah/yq/releases
ARG YQ_VERSION=v4.35.2
ARG YQ_SHA256=5b440478ab0bb87c4e924b2db549fa9e88ab1042a5003e0fe2201010f2d911a2
RUN cd /tmp && \
    curl -sSL "https://github.com/mikefarah/yq/releases/download/${YQ_VERSION}/yq_linux_amd64.tar.gz" -o yq.tar.gz && \
    echo "${YQ_SHA256}  yq.tar.gz" | sha256sum -c - && \
    tar xzvf yq.tar.gz && \
    mv yq_linux_amd64 /usr/local/bin/yq && \
    rm -rf /tmp/*

# Download and verify Helm
# https://github.com/helm/helm/releases
ARG HELM_VERSION=3.13.2
ARG HELM_SHA256=55a8e6dce87a1e52c61e0ce7a89bf85b38725ba3e8deb51d4a08ade8a2c70b2d
RUN cd /tmp && \ 
    curl "https://get.helm.sh/helm-v${HELM_VERSION}-linux-amd64.tar.gz" -o helm.tar.gz && \
    echo "${HELM_SHA256}  helm.tar.gz" | sha256sum -c - && \
    tar xzvf helm.tar.gz --strip-components=1 && \
    mv helm /usr/local/bin/ && \
    rm -rf /tmp/*

RUN oc completion bash > /etc/bash_completion.d/oc_completion && \
    yq shell-completion bash > /etc/bash_completion.d/yq_completion && \
    mkdir -p /root/.kube
